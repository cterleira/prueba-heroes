import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroService } from 'src/app/services/hero.service';
import { HeroeData } from '../../model/heroe.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-heroe-edit',
  templateUrl: './heroe-edit.component.html',
  styleUrls: ['./heroe-edit.component.css'],
})
export class HeroeEditComponent implements OnInit {
  public heroe: HeroeData | undefined;

  editHeroeForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', Validators.required),
    img: new FormControl('', [Validators.pattern("^[a-zA-Z0-9._%+-:/]+\\.(jpeg|jpg|gif|png)$")]),
  });

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    public router: Router,
  ) {}

  ngOnInit(): void {
    this.getHero();
    this.setForm();
  }

  getHero(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.heroService.getHero(id).subscribe((heroe) => (this.heroe = heroe));
  }

  setForm(){
    this.editHeroeForm.get('name')?.setValue(this.heroe?.name);
    this.editHeroeForm.get('description')?.setValue(this.heroe?.description);
    this.editHeroeForm.get('img')?.setValue(this.heroe?.img);
  }

  editHeroe() {
    if (this.heroe) {
      const heroeAux = {
        id: this.heroe?.id,
        name: this.editHeroeForm.get('name')?.value,
        description: this.editHeroeForm.get('description')?.value,
        date: this.editHeroeForm.get('date')?.value,
        img: this.editHeroeForm.get('img')?.value,
      };

      this.heroService.editHero(heroeAux);
      this.router.navigate(['/heroes']);
    }
  }
}
