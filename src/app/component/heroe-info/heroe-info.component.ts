import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroService } from 'src/app/services/hero.service';
import { HeroeData } from '../../model/heroe.model';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-heroe-info',
  templateUrl: './heroe-info.component.html',
  styleUrls: ['./heroe-info.component.css']
})
export class HeroeInfoComponent implements OnInit {

  public hero: HeroeData | undefined ;
  public showInfo: boolean = true;

  editHeroeForm = new FormGroup({
    name: new FormControl(''),
    lastName: new FormControl(''),
  });

  constructor(private route: ActivatedRoute,private heroService: HeroService) { }

  ngOnInit(): void {
    this.getHero();
  }

  getHero(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.heroService.getHero(id)
      .subscribe(hero => this.hero = hero);
  }

  onSubmit() {
    console.warn(this.editHeroeForm.value);
  }

}
