import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { HeroeData } from 'src/app/model/heroe.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ModalComponent } from 'src/app/shared/modal/modal.component';
import { MatDialog } from '@angular/material/dialog';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.css'],
})
export class HeroesListComponent implements OnInit {
  @Input() heroeslist: HeroeData[] = [];
  public heroeslistAux: HeroeData[] = [];
  @Output() idHero: EventEmitter<number> = new EventEmitter();

  filterHeroeForm = new FormGroup({
    name: new FormControl(''),
  });

  dataSource: any;
  displayedColumns: string[] = ['id', 'name', 'description', 'date', 'boton'];
  @ViewChild(MatPaginator, { static: true }) paginator:
    | MatPaginator
    | undefined;
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {
    this.heroeslistAux = this.heroeslist;
    this.initTable(this.heroeslistAux);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.heroeslistAux = changes.heroeslist.currentValue;
      this.initTable(this.heroeslistAux);
    }
  }

  initTable(heroeslistAux: HeroeData[]) {
    this.dataSource = new MatTableDataSource<HeroeData>(this.heroeslistAux);
    this.dataSource.paginator = this.paginator;
  }

  openDialog(id: number, name: string): void {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '250px',
      data: { name: name },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.idHero.emit(id);
      }
    });
  }

  public filter(event: any) {
    this.heroeslistAux =
      event.target.value !== ''
        ? this.heroeslist.filter((s) => s.name.includes(event.target.value))
        : this.heroeslist;
    this.initTable(this.heroeslistAux);
  }
}
