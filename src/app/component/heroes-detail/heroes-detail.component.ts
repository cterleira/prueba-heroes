import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ModalComponent } from '../../shared/modal/modal.component';

@Component({
  selector: 'app-heroes-detail',
  templateUrl: './heroes-detail.component.html',
  styleUrls: ['./heroes-detail.component.css'],
})
export class HeroesDetailComponent implements OnInit {
  @Input() hero: any;
  @Output() idHero: EventEmitter<number> = new EventEmitter();

  constructor(public dialog: MatDialog, public router: Router) {}

  ngOnInit(): void {}

  editHeroe() {
    this.router.navigate(['/edit/' + this.hero.id]);
  }

  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent, {
      width: '250px',
      data: { name: this.hero.name },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.idHero.emit(this.hero.id);
      }
    });
  }
}
