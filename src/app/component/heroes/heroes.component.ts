import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HeroeData } from '../../model/heroe.model';
import { HeroService } from '../../services/hero.service';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css'],
})
export class HeroesComponent implements OnInit {
  public heroes: HeroeData[] = [];
  public heroeslist: HeroeData[] = [];
  public showSpinner: boolean = false;

  buscarHeroeForm = new FormGroup({
    name: new FormControl(''),
  });

  @ViewChild(MatPaginator, { static: true }) paginator:
    | MatPaginator
    | undefined;

  constructor(private heroService: HeroService) {}

  ngOnInit(): void {
    this.getHeroes();
  }

  public getHeroes(): void {
    this.heroService
      .getHeroes()
      .subscribe((heroes) => (this.heroes = heroes.slice(0, heroes.length)));
    this.heroeslist = this.heroes;
  }

  public getHeroName(event: any): void {
    this.heroService
      .getHeroName(event.target.value)
      .subscribe((heroes) => (this.heroes = heroes.slice(0, heroes.length)));
    this.heroeslist = this.heroes;
  }

  public removeItemFromArr(idHero: number) {
    this.heroService
      .deleteHero(idHero)
      .subscribe((heroes) => (this.heroes = heroes.slice(0, heroes.length)));
    this.heroeslist = this.heroes;
  }
}
