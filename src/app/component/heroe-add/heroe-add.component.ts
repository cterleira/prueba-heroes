import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HeroService } from 'src/app/services/hero.service';



@Component({
  selector: 'app-heroe-add',
  templateUrl: './heroe-add.component.html',
  styleUrls: ['./heroe-add.component.css']
})


export class HeroeAddComponent implements OnInit {


  addHeroeForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    description: new FormControl('', Validators.required),
    img: new FormControl('', [Validators.pattern("^[a-zA-Z0-9._%+-:/]+\\.(jpeg|jpg|gif|png)$")]),

  });

  todayDate : Date = new Date();

  constructor(private heroService: HeroService, public router: Router,) { }

  ngOnInit(): void {
  }

  public addHeroe() {
    const heroeAux = {
      name : this.addHeroeForm.get('name')?.value,
      description : this.addHeroeForm.get('description')?.value,
      date : this.todayDate,
      img : (this.addHeroeForm.get('img')?.value)?this.addHeroeForm.get('img')?.value:'./assets/img/image_not_available.jpg',
    }
    this.heroService.addHero(heroeAux);
    this.router.navigate(['/heroes']);
  }

}
