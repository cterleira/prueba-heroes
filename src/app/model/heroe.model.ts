export interface HeroeData {
  id: number;
  name: string;
  description?: string;
  date?: Date | string;
  img?: string;
}

export interface HeroeList {
  id: number;
  name: string;
  description: string;
  date: string;
  img: img;
}

export interface DialogData {
  id?: number;
  name: string;
}
interface img {
  url?: string;
  alt?: string;
}
