import { Component, HostListener } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Prueba técnica frontend - Heroes';
  showSpinner: boolean = false;
  autoUnsubscribe$: Observable<any> | undefined;
  constructor(){}

  ngOnInit(): void {

  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this._setMobile();
  }

  private _setMobile() {
    var mobile = window.innerWidth >= 1024 ? false : true;
    console.log('mobile:' ,mobile);
  }

}
