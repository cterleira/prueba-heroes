
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroeAddComponent } from './component/heroe-add/heroe-add.component';
import { HeroeEditComponent } from './component/heroe-edit/heroe-edit.component';
import { HeroeInfoComponent } from './component/heroe-info/heroe-info.component';
import { HeroesComponent } from './component/heroes/heroes.component';

const routes: Routes = [
  { path: 'heroes', component: HeroesComponent },
  { path: 'addHeroe', component: HeroeAddComponent },
  { path: 'detail/:id', component: HeroeInfoComponent },
  { path: 'edit/:id', component: HeroeEditComponent },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
