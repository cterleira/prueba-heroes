import { Injectable } from '@angular/core';
import { HeroeData } from '../model/heroe.model';
import { HEROES } from '../mock/mock-heroes';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class HeroService {
  constructor(private http: HttpClient) {}

  getHeroes(): Observable<HeroeData[]> {
    const heroes = of(HEROES);
    return heroes;
  }

  getHero(id: number): Observable<HeroeData> {
    const hero = HEROES.find((h) => h.id === id)!;
    return of(hero);
  }

  getHeroName(name: string): Observable<HeroeData[]> {
    const heroes =
      name !== ''
        ? of(HEROES.filter((s) => s.name.includes(name)))
        : of(HEROES);
    return heroes;
  }

  deleteHero(id: number): Observable<HeroeData[]> {
    HEROES.splice(
      HEROES.findIndex((v) => v.id === id),
      1
    );
    const heroes = of(HEROES);

    return heroes;
  }

  editHero(heroeAux: {
    id: number;
    name: string;
    description: string;
    date: Date;
    img: string;
  }) {
    HEROES.forEach(function (obj) {
      if (obj.id === heroeAux.id) {
        obj.name = heroeAux.name;
        obj.description = heroeAux.description;
        obj.date = heroeAux.date;
        obj.img = heroeAux.img;
      }
    });
  }

  addHero(heroeAux: {
    name: string;
    description: string;
    date: Date;
    img: string;
  }) {
    var addHeroe = {
      id: HEROES.length + 1,
      name: heroeAux.name,
      description: heroeAux.description,
      date: heroeAux.date,
      img: heroeAux.img,
    };
    HEROES.push(addHeroe);
  }
}
