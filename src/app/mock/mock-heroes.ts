import { HeroeData } from '../model/heroe.model';

export const HEROES: HeroeData[] = [
  {
    id: 0,
    name: '3-D Man',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vulputate nisl arcu, vel pellentesque sem tincidunt vitae. Curabitur pellentesque pretium imperdiet.',
    date: '2013-09-18',
    img: 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg',
  },
  {
    id: 1,
    name: 'A-Bomb (HAS)',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vulputate nisl arcu, vel pellentesque sem tincidunt vitae. Curabitur pellentesque pretium imperdiet.',
    date: '2013-09-18',
    img: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg',
  },
  {
    id: 2,
    name: 'A.I.M.',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vulputate nisl arcu, vel pellentesque sem tincidunt vitae. Curabitur pellentesque pretium imperdiet.',
    date: '2013-09-18',
    img: 'http://i.annihil.us/u/prod/marvel/i/mg/6/20/52602f21f29ec.jpg',
  },
  {
    id: 3,
    name: 'Spider-Man',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vulputate nisl arcu, vel pellentesque sem tincidunt vitae. Curabitur pellentesque pretium imperdiet.',
    date: '2013-09-18',
    img: 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/526548a343e4b.jpg',
  },
  {
    id: 4,
    name: 'Agent Zero',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vulputate nisl arcu, vel pellentesque sem tincidunt vitae. Curabitur pellentesque pretium imperdiet.',
    date: '2013-09-18',
    img: 'http://i.annihil.us/u/prod/marvel/i/mg/f/60/4c0042121d790.jpg',
  },
  {
    id: 5,
    name: 'Adam Warlock',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vulputate nisl arcu, vel pellentesque sem tincidunt vitae. Curabitur pellentesque pretium imperdiet.',
    date: '2013-09-18',
    img: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg',
  }
];
